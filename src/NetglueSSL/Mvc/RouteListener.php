<?php
/**
 * Route Listener
 * 
 * Retrieves information from the matched route and uses the UriResolver to help determine
 * whether a redirect should occur and where that uri should be
 */

namespace NetglueSSL\Mvc;

use Zend\EventManager\ListenerAggregateInterface;

use Zend\EventManager\EventManagerInterface;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Application;

use Zend\Http\Request as HttpRequest;

use NetglueSSL\Service\UriResolver;

class RouteListener implements ListenerAggregateInterface {
	
	/**
	 * Event Listeners
	 * @var array
	 */
	protected $listeners = array();
	
	/**
	 * UriResolver
	 * @var UriResolver
	 */
	protected $resolver;
	
	/**
	 * Attach to an event manager
	 *
	 * @param  EventManagerInterface $events
	 * @return void
	 */
	public function attach(EventManagerInterface $events) {
		$this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onRoute'));
	}
	
	/**
	 * Detach all our listeners from the event manager
	 *
	 * @param EventManagerInterface $events
	 * @return void
	 */
	public function detach(EventManagerInterface $events) {
		foreach ($this->listeners as $index => $listener) {
			if ($events->detach($listener)) {
				unset($this->listeners[$index]);
			}
		}
	}
	
	/**
	 * Listen to the route event, figure out whether we should redirect to the http or https host or not and perform the redirection if appropriate
	 * @param  MvcEvent $e
	 * @return Zend\Http\Response|void
	 */
	public function onRoute($e) {
		if($e->isError() && $e->getError() === Application::ERROR_ROUTER_NO_MATCH) {
			// No matched route has been found - don't do anything
			return;
		}
		$request = $e->getRequest();
		$response = $e->getResponse();
		
		if(!$request instanceof HttpRequest) {
			// Not an HTTP Request
			return;
		}
		
		$router = $e->getRouter();
		$match = $e->getRouteMatch();
		$routeName = $match->getMatchedRouteName();
		$params = $match->getParams();
		
		/**
		 * If we have a route tht defines 'force-ssl' prefer that instruction above
		 * anything else and redirect if appropriate
		 * 
		 * Possible values of 'force-ssl' param are:
		 *   true|'ssl' : Force SSL
		 *   'http'     : Force Non-SSL
		 *   false      : Don't do anything and skip any further evaluation
		 */
		if(isset($params['force-ssl'])) {
			$force = strtolower($params['force-ssl']);
			if( ($force === true || $force === 'ssl') && $this->getResolver()->isHttp($request) ) {
				$uri = $this->getResolver()->getSslUri($request);
				return $this->redirect($uri, $response);
			}
			if( $force === 'http' && $this->getResolver()->isSSL($request) ) {
				$uri = $this->getResolver()->getHttpUri($request);
				return $this->redirect($uri, $response);
			}
			if($force === false) {
				// Don't continue to evaluate controllers, uris etc
				return;
			}
		}
		
		// Redirect based on controller?
		$controller = $match->getParam('controller');
		if( $uri = $this->getResolver()->getControllerRedirectUri($request, $controller)) {
			return $this->redirect($uri, $response);
		}
		
		// Redirect based on route name?
		if( $uri = $this->getResolver()->getRouteRedirectUri($request, $routeName)) {
			return $this->redirect($uri, $response);
		}
		
		// Redirect based on uri?
		if($uri = $this->getResolver()->getUriRedirectUri($request)) {
			return $this->redirect($uri, $response);
		}
		return;
	}
	
	/**
	 * Modify the response to perform a redirect
	 * @param string|Zend\Uri\Http
	 * @param Zend\Http\Response $response
	 * @return Zend\Http\Response
	 */
	protected function redirect($uri, $response) {
		$response->getHeaders()->addHeaderLine('Location', $uri);
		$response->setStatusCode(302);
		return $response;
	}
	
	/**
	 * Return the URI Resolver Service
	 * @return UriResolver|NULL
	 */
	public function getResolver() {
		return $this->resolver;
	}
	
	/**
	 * Set the URI Resolver Service
	 * @param UriResolver
	 * @return ForceSsl
	 */
	public function setResolver(UriResolver $resolver) {
		$this->resolver = $resolver;
		return $this;
	}
	
}