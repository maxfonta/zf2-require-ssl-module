<?php
/**
 * URI Resolver Service mostly helps us to figure out the corresponding http/s uri for a given request
 * and helps determine whether we should redirect or not
 *
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */

namespace NetglueSSL\Service;

use NetglueSSL\Service\Options;

use Zend\Uri\Http as HttpUri;
use Zend\Uri\UriFactory;
use Zend\Uri\Excpetion\ExceptionInterface as UriException;

use Zend\Http\Request as HttpRequest;

class UriResolver {
	
	/**
	 * Options
	 * @var Options
	 */
	protected $options;
	
	/**
	 * Require options
	 * @param Options $options
	 * @return void
	 */
	public function __construct(Options $options) {
		$this->options = $options;
	}
	
	/**
	 * Return options
	 * @return Options
	 */
	public function getOptions() {
		return $this->options;
	}
	
	/**
	 * Return the SSL Uri for the given URI
	 * @param mixed $uri
	 * @return HttpUri
	 */
	public function getSslUri($uri) {
		if($this->isSSL($uri)) {
			return $this->getUri($uri);
		}
		$uri = $this->requireUri($uri);
		$path = $uri->getPath();
		$host = $this->options->getSslHostname() ? $this->options->getSslHostname() : $uri->getHost();
		$scheme = 'https';
		
		// Strip any HTTP path prefix
		if($this->options->getHttpPathPrefix()) {
			$prefix = $this->options->getHttpPathPrefix();
			if(strpos($path, $prefix) === 0) {
				$path = substr($path, strlen($prefix));
			}
		}
		
		// Prepend the the https path prefix
		if($this->options->getSslPathPrefix()) {
			$path = $this->options->getSslPathPrefix().$path;
		}
		
		$port = $this->options->getSslPort();
		
		$result = clone($uri);
		$result->setHost($host)
		       ->setScheme($scheme)
		       ->setPath($path)
		       ->setPort($port);
		$result->normalize();
		return $result;
	}
	
	/**
	 * Return the standard HTTP Uri that corresponds to the given SSL Uri
	 * @param mixed $uri
	 * @return HttpUri
	 */
	public function getHttpUri($uri) {
		if($this->isHttp($uri)) {
			return $this->getUri($uri);
		}
		$uri = $this->requireUri($uri);
		$path = $uri->getPath();
		$host = $this->options->getHttpHostname() ? $this->options->getHttpHostname() : $uri->getHost();
		$scheme = 'http';
		
		// Strip any SSL path prefix
		if($this->options->getSslPathPrefix()) {
			$prefix = $this->options->getSslPathPrefix();
			if(strpos($path, $prefix) === 0) {
				$path = substr($path, strlen($prefix));
			}
		}
		
		// Prepend the the http path prefix if any
		if($this->options->getHttpPathPrefix()) {
			$path = $this->options->getHttpPathPrefix().$path;
		}
		
		$port = $this->options->getHttpPort();
		
		$result = clone($uri);
		$result->setHost($host)
		       ->setScheme($scheme)
		       ->setPath($path)
		       ->setPort($port);
		$result->normalize();
		return $result;
	}
	
	/**
	 * Helper to return a Zend\Uri\Http instance from various arguments
	 * @param mixed $uri
	 * @return HttpUri|false
	 */
	public function getUri($uri) {
		if($uri instanceof HttpRequest) {
			return $uri->getUri();
		}
		if(is_string($uri)) {
			return UriFactory::factory($uri);
		}
		if($uri instanceof HttpUri) {
			return $uri;
		}
		return false;
	}
	
	/**
	 * Require that the given uri can be resolved to an HttpUri instance
	 * @param mixed $uri
	 * @return HttpUri
	 * @throws InvalidArgumentException if we cannot retrieve a uri instance
	 * @see getUri()
	 */
	public function requireUri($uri) {
		// Catch exceptions thrown by the uri constructor or factory
		$e = NULL;
		try {
			$instance = $this->getUri($uri);
		} catch(UriException $e) {
			$instance = false;
		}
		if(false === $instance) {
			throw new \InvalidArgumentException(
				sprintf(
					'Expected a string, an instanceof Zend\Uri\Http or something else able to return a Uri instance got %s',
					is_object($uri) ? get_class($uri) : gettype($uri)
				),
				NULL,
				$e
			);
		}
		return $instance;
	}
	
	/**
	 * Whether the given uri represents an encrypted connection
	 * @param mixed $uri
	 * @return bool
	 */
	public function isSSL($uri) {
		$uri = $this->requireUri($uri);
		return ($uri->getScheme() === 'https' ||
                (array_key_exists('HTTP_X_SSL', $_SERVER) && $_SERVER['HTTP_X_SSL']));
	}
	
	/**
	 * Whether the given uri represents a standard http connection
	 * @param mixed $uri
	 * @return bool
	 */
	public function isHttp($uri) {
		return !$this->isSSL($uri);
	}
	
	/**
	 * Return the redirect uri for the given controller in the context of the given uri
	 * @param mixed $uri
	 * @param string $controller
	 * @return HttpUri|NULL If a uri is returned, it is expected that a redirect should occur
	 */
	public function getControllerRedirectUri($uri, $controller) {
		if(!is_string($controller) || empty($controller)) {
			return;
		}
		if($this->isSSL($uri)) {
			$search = $this->options->getHttpControllers();
			if(in_array($controller, $search)) {
				return $this->getHttpUri($uri);
			}
		} else {
			$search = $this->options->getSslControllers();
			if(in_array($controller, $search)) {
				return $this->getSslUri($uri);
			}
		}
	}
	
	/**
	 * Return the redirect uri for the given route name in the context of the given uri
	 * @param mixed $uri
	 * @param string $route
	 * @return HttpUri|NULL If a uri is returned, it is expected that a redirect should occur
	 */
	public function getRouteRedirectUri($uri, $route) {
		if(!is_string($route) || empty($route)) {
			return;
		}
		if($this->isSSL($uri)) {
			$search = $this->options->getHttpRoutes();
			if(in_array($route, $search)) {
				return $this->getHttpUri($uri);
			}
		} else {
			$search = $this->options->getSslRoutes();
			if(in_array($route, $search)) {
				return $this->getSslUri($uri);
			}
		}
	}
	
	/**
	 * Return the redirect Uri for the given uri
	 * @param mixed $uri
	 * @return HttpUri|NULL If a uri is returned, it is expected that a redirect should occur
	 */
	public function getUriRedirectUri($uri) {
		$uri = $this->requireUri($uri);
		if($this->isSSL($uri)) {
			$search = $this->options->getHttpUris();
			if($this->isUriMatch($uri->getPath(), $search)) {
				return $this->getHttpUri($uri);
			}
		} else {
			$search = $this->options->getSslUris();
			if($this->isUriMatch($uri->getPath(), $search)) {
				return $this->getSslUri($uri);
			}
		}
	}
	
	/**
	 * Whether the given path matches against any of the configured uri patterns
	 * @param string $path
	 * @param array $patterns
	 * @return bool
	 */
	protected function isUriMatch($path, array $patterns) {
		foreach($patterns as $pattern) {
			$pattern = '/^'.preg_quote($pattern, '/').'/';
			if(preg_match($pattern, $path)) {
				return true;
			}
		}
		return false;
	}
	
	
}