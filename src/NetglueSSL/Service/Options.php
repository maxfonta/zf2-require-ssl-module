<?php

namespace NetglueSSL\Service;

use Zend\StdLib\AbstractOptions;

class Options extends AbstractOptions {
	
	protected $enable = true;
	protected $sslHost;
	protected $httpHost;
	protected $sslPort;
	protected $httpPort;
	protected $sslPathPrefix;
	protected $httpPathPrefix;
	
	protected $sslOnly = array();
	protected $httpOnly = array();
	
	public function setSslHostname($host) {
		$this->sslHost = trim(strtolower($host));
		return $this;
	}
	
	public function setHttpHostname($host) {
		$this->httpHost = trim(strtolower($host));
		return $this;
	}
	
	public function setSslPort($port) {
		$port = is_numeric($port) ? (int) $port : NULL;
		$this->sslPort = $port;
		return $this;
	}
	
	public function setHttpPort($port) {
		$port = is_numeric($port) ? (int) $port : NULL;
		$this->httpPort = $port;
		return $this;
	}
	
	public function setSslPathPrefix($prefix) {
		$this->sslPathPrefix = $prefix;
		return $this;
	}
	
	public function setHttpPathPrefix($prefix) {
		$this->httpPathPrefix = $prefix;
		return $this;
	}
	
	public function setSslOnly(array $config) {
		$this->sslOnly = $config;
		return $this;
	}
	
	public function setHttpOnly(array $config) {
		$this->httpOnly = $config;
		return $this;
	}
	
	public function getSslHostname() {
		return $this->sslHost;
	}
	
	public function getHttpHostname() {
		return $this->httpHost;
	}
	
	public function getSslPathPrefix() {
		return $this->sslPathPrefix;
	}
	
	public function getHttpPathPrefix() {
		return $this->httpPathPrefix;
	}
	
	public function getSslPort() {
		return $this->sslPort;
	}
	
	public function getHttpPort() {
		return $this->httpPort;
	}
	
	public function getSslControllers() {
		$conf = $this->sslOnly;
		if(isset($conf['controllers'])) {
			return $conf['controllers'];
		}
		return array();
	}
	
	public function getSslRoutes() {
		$conf = $this->sslOnly;
		if(isset($conf['routes'])) {
			return $conf['routes'];
		}
		return array();
	}
	
	public function getSslUris() {
		$conf = $this->sslOnly;
		if(isset($conf['uris'])) {
			return $conf['uris'];
		}
		return array();
	}
	
	public function getHttpControllers() {
		$conf = $this->httpOnly;
		if(isset($conf['controllers'])) {
			return $conf['controllers'];
		}
		return array();
	}
	
	public function getHttpRoutes() {
		$conf = $this->httpOnly;
		if(isset($conf['routes'])) {
			return $conf['routes'];
		}
		return array();
	}
	
	public function getHttpUris() {
		$conf = $this->httpOnly;
		if(isset($conf['uris'])) {
			return $conf['uris'];
		}
		return array();
	}
	
	public function setEnable($flag) {
		$this->enable = (bool) $flag;
		return $this;
	}
	
	public function getEnable() {
		return $this->enable;
	}
	
	public function isEnabled() {
		return $this->getEnable();
	}
	
}