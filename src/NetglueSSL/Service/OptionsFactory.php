<?php
/**
 * Factory to return an options instance
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */

namespace NetglueSSL\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetglueSSL\Service\Options;

class OptionsFactory implements FactoryInterface {
	
	/**
	 * Create Options instance from config
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return Options
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		$options = isset($config['netglue_ssl']['options']) ?
			$config['netglue_ssl']['options'] :
			array();
		
		return new Options($options);
	}
	
}